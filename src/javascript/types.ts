export interface Modal {
    readonly title: string;
    readonly bodyElement: HTMLElement | string;
    onClose?: () => void;
}

type ID = string | number

export interface Fighter {
    _id: ID;
    name: string;
    health?: number;
    attack?: number;
    defense?: number;
    blocking?: boolean;
    lastCrit?: number;
    maxHealth?: number;
    source: string;
}

export interface Dictionary {
    [index: string]: string
}

export interface ImageAttributes {
    src: string;
    title: string;
    alt: string;
}

export interface DOMElementSettings {
    tagName: string;
    className?: string;
    attributes?: Dictionary | ImageAttributes;
}

export { ID };