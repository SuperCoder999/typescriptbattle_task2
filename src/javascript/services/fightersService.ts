import { callApi } from '../helpers/apiHelper';
import { ID, Fighter } from '../types';

class FighterService {
    async getFighters(): Promise<Fighter[]> {
        try {
            const endpoint = 'fighters.json';
            const apiResult: Fighter[] = (await callApi(endpoint, 'GET') as Fighter[]);

            return apiResult;
        } catch (error) {
            console.log("Error in getFighters:", error.name, "-", error.message);
            throw error;
        }
    }

    async getFighterDetails(id: ID): Promise<Fighter> {
        try {
            const endpoint = `details/fighter/${id}.json`;
            const apiResult: Fighter = (await callApi(endpoint, 'GET') as Fighter);

            return apiResult;
        } catch (error) {
            console.log("Error in getFighterDetails:", error.name, "-", error.message);
            throw error;
        }
    }
}

export const fighterService = new FighterService();
