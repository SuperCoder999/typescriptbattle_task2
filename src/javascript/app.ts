import { createFighters } from './components/fightersView';
import { fighterService } from './services/fightersService';
import { Fighter } from './types';

class App {
    constructor() {
        this.startApp();
    }

    static rootElement: HTMLElement = <HTMLElement>document.getElementById('root');
    static loadingElement: HTMLElement = <HTMLElement>document.getElementById('loading-overlay');

    async startApp(): Promise<void> {
        try {
            App.loadingElement.style.visibility = 'visible';

            const fighters: Fighter[] = await fighterService.getFighters();
            const fightersElement: HTMLElement = createFighters(fighters);

            App.rootElement.appendChild(fightersElement);
        } catch (error) {
            console.warn(error);
            App.rootElement.innerText = 'Failed to load data';
        } finally {
            App.loadingElement.style.visibility = 'hidden';
        }
    }
}

export default App;
