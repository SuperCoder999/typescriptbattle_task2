import { fightersDetails, fighters } from './mockData';
import { Fighter, ID } from '../types';

const API_URL = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI = true;

// eslint-disable-next-line @typescript-eslint/ban-types
async function callApi(endpoint: string, method: string): Promise<Object> {
    const url: string = API_URL + endpoint;

    const options: { method: string } = {
        method,
    };

    return useMockAPI
        ? fakeCallApi(endpoint)
        : fetch(url, options)
            .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
            .then((result) => JSON.parse(atob(result.content)))
            .catch((error) => {
                throw error;
            });
}

async function fakeCallApi(endpoint: string) : Promise<Fighter | Fighter[]> {
    const response: void | Fighter | Fighter[] = endpoint === 'fighters.json' ? fighters : getFighterById(endpoint);

    return new Promise((resolve, reject) => {
        setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
    });
}

function getFighterById(endpoint: string): Fighter | void {
    const start: number = endpoint.lastIndexOf('/');
    const end: number = endpoint.lastIndexOf('.json');
    const id: ID = endpoint.substring(start + 1, end);

    return fightersDetails.find(it => it._id === id);
}

export { callApi };
