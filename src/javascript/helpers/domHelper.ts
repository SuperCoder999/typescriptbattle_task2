import { DOMElementSettings, Dictionary } from '../types';

export function createElement({ tagName, className = '', attributes = {} }: DOMElementSettings): HTMLElement {
    const element: HTMLElement = document.createElement(tagName);

    if (className) {
        const classNames: string[] = className.split(' ').filter(Boolean);
        element.classList.add(...classNames);
    }

    Object.keys(attributes).forEach((key) => element.setAttribute(key, (attributes as Dictionary)[key]));

    return element;
}
