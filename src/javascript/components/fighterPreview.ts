import { createElement } from '../helpers/domHelper';
import { ImageAttributes, Fighter } from '../types';

export function createFighterPreview(fighter: Fighter, position: 'left' | 'right'): HTMLElement {
    const positionClassName: string = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';

    const fighterElement: HTMLElement = createElement({
        tagName: 'div',
        className: `fighter-preview___root ${positionClassName}`,
    });

    const image: HTMLElement = createFighterImage(fighter);
    const textName = createFighterInfoText(fighter.name.toUpperCase());
    const textAttack = createFighterInfoText(`Attack: ${fighter.attack}`);
    const textDefense = createFighterInfoText(`Defense: ${fighter.defense}`);
    const textHealth = createFighterInfoText(`Health: ${fighter.health}`);

    fighterElement.append(
        textName,
        textAttack,
        textDefense,
        textHealth,
        image
    );

    return fighterElement;
}

export function createFighterImage(fighter: Fighter): HTMLElement {
    const { source, name }: { source: string, name: string } = fighter;

    const attributes: ImageAttributes = { 
        src: source, 
        title: name,
        alt: name 
    };

    const imgElement: HTMLElement = createElement({
        tagName: 'img',
        className: 'fighter-preview___img',
        attributes,
    });

    return imgElement;
}

export function createFighterInfoText(text: string): HTMLElement {
    const textElement: HTMLElement = createElement({
        tagName: 'div',
        className: `fighter-preview___info`,
    });

    textElement.innerText = text;

    return textElement;
}
