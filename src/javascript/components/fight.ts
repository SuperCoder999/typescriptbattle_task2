import { Controls } from '../../constants/controls';
import { Fighter } from '../types';

const MS_TO_SEC_DIVIDER = 1000;

const performanceExt = Object.assign(performance, {
    nowSec: function (): number {
        return performance.now() / MS_TO_SEC_DIVIDER;
    }
});

export async function fight(firstFighterTemp: Fighter, secondFighterTemp: Fighter): Promise<Fighter | { name: string }> {
    return new Promise(resolve => {
        const fightStart: number = performanceExt.nowSec();
        const firstFighter = Object.assign({ lastCrit: -10, maxHealth: firstFighterTemp.health! }, firstFighterTemp);
        const secondFighter = Object.assign({ lastCrit: -10, maxHealth: secondFighterTemp.health! }, secondFighterTemp);
        const firstFighterHealthBar: HTMLElement = <HTMLElement>document.getElementById('left-fighter-indicator');
        firstFighterHealthBar.innerText = <string><unknown>firstFighter.health!;
        const secondFighterHealthBar: HTMLElement = <HTMLElement>document.getElementById('right-fighter-indicator');
        secondFighterHealthBar.innerText = <string><unknown>secondFighter.health!;
        const pressed: Set<string> = new Set();

        const KeyPress: (event: KeyboardEvent) => void = function (event: KeyboardEvent): void {
            pressed.add(event.code);
            pressed.forEach((code: string) => checkNormalActions(code, firstFighter, secondFighter));

            checkCriticalCombo(
                pressed,
                firstFighter,
                secondFighter,
                fightStart
            );

            setHealthBars(
                firstFighterHealthBar,
                secondFighterHealthBar,
                firstFighter,
                secondFighter
            );

            const winner: Fighter | void | { name: string } = getWinner(firstFighter, secondFighter);

            if (winner) {
                document.onkeypress = null;
                return resolve(winner);
            }
        }

        const KeyUp: (event: KeyboardEvent) => void = (event: KeyboardEvent) => void pressed.delete(event.code);

        document.onkeypress = KeyPress;
        document.onkeyup = KeyUp;
    });
}

export function getWinner(fighter1: Fighter, fighter2: Fighter): Fighter | { name: string } | void {
    if (fighter1.health! <= 0 && fighter2.health! <= 0) {
        return { name: fighter1.name + ' and ' + fighter2.name };
    }

    if (fighter1.health! <= 0) {
        return fighter2;
    }

    if (fighter2.health! <= 0) {
        return fighter1;
    }
}

export function setHealthBars(
    bar1: HTMLElement,
    bar2: HTMLElement,
    fighter1: Fighter,
    fighter2: Fighter
): void {
    bar1.style.width = <string><unknown>(Math.floor(fighter1.health! / fighter1.maxHealth! * 100)) + '%';
    bar1.innerText = <string><unknown>fighter1.health!;
    bar2.style.width = <string><unknown>(Math.floor(fighter2.health! / fighter2.maxHealth! * 100)) + '%';
    bar2.innerText = <string><unknown>fighter2.health!;
}

export function checkCriticalCombo(
    pressed: Set<string>,
    firstFighter: Fighter,
    secondFighter: Fighter,
    fightStart: number
): void {
    const firstPlayerCombo: boolean = pressed.has(Controls.PlayerOneCriticalHitCombination[0]) &&
                                    pressed.has(Controls.PlayerOneCriticalHitCombination[1]) &&
                                    pressed.has(Controls.PlayerOneCriticalHitCombination[2]);

    const secondPlayerCombo: boolean = pressed.has(Controls.PlayerTwoCriticalHitCombination[0]) &&
                                    pressed.has(Controls.PlayerTwoCriticalHitCombination[1]) &&
                                    pressed.has(Controls.PlayerTwoCriticalHitCombination[2]);

    const fightTime: number = performanceExt.nowSec() - fightStart;
    const canFirstFighterCrit: boolean = firstPlayerCombo && fightTime - firstFighter.lastCrit! >= 10;

    if (canFirstFighterCrit) {
        crit(firstFighter, secondFighter, fightStart);
    }

    const canSecondFighterCrit: boolean = secondPlayerCombo && fightTime - secondFighter.lastCrit! >= 10;

    if (canSecondFighterCrit) {
        crit(secondFighter, firstFighter, fightStart);
    }
}

export function checkNormalActions(code: string, firstFighter: Fighter, secondFighter: Fighter): void {
    switch (code) {
        case Controls.PlayerOneAttack:
            attack(firstFighter, secondFighter);
            break;
        case Controls.PlayerTwoAttack:
            attack(secondFighter, firstFighter);
            break;
        case Controls.PlayerOneBlock:
            setBlock(firstFighter, true);
            break;
        case Controls.PlayerTwoBlock:
            setBlock(secondFighter, true);
            break;
    }

    if (!(code === Controls.PlayerOneBlock)) {
        setBlock(firstFighter, false);
    }

    if (!(code === Controls.PlayerTwoBlock)) {
        setBlock(secondFighter, false);
    }
}

export function crit(attacker: Fighter, defender: Fighter, fightStart: number): void {
    defender.health! -= getCritPower(attacker);
    attacker.lastCrit! = performanceExt.nowSec() - fightStart;
}

export function attack(attacker: Fighter, defender: Fighter): void {
    const canAttack = !defender.blocking && !attacker.blocking;

    if (canAttack) {
        defender.health! -= getDamage(attacker, defender);
    }
}

export function setBlock(defender: Fighter, block: boolean): void {
    defender.blocking = block;
}

export function getDamage(attacker: Fighter, defender: Fighter): number {
    return Math.max(0, getHitPower(attacker) - getBlockPower(defender));
}

export function chance(): number {
    return Math.random() > 0.5 ? 2 : 1;
}

export function getHitPower(fighter: Fighter): number {
    return chance() * fighter.attack!;
}

export function getCritPower(fighter: Fighter): number {
    return fighter.attack! * 2;
}

export function getBlockPower(fighter: Fighter): number {
    return chance() * fighter.defense!;
}
