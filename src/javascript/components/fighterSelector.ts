import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { Fighter, ID } from '../types';

import { fighterService } from '../services/fightersService';

export function createFightersSelector(): (event: MouseEvent, fighterId: ID) => void {
    let selectedFighters: Fighter[] = [];

    return async (event: MouseEvent, fighterId: ID) => {
        const fighter: Fighter = await getFighterInfo(fighterId);
        const [playerOne, playerTwo]: Fighter[] = selectedFighters;
        const firstFighter: Fighter = playerOne ?? fighter;
        const secondFighter: Fighter = playerOne ? playerTwo ?? fighter : playerTwo;
        selectedFighters = [firstFighter, secondFighter];

        renderSelectedFighters(selectedFighters);
    };
}

const fighterDetailsMap: Map<ID, Fighter> = new Map();

export async function getFighterInfo(fighterId: ID): Promise<Fighter> {
    if (!fighterDetailsMap.get(fighterId)) {
        const details: Fighter = await fighterService.getFighterDetails(fighterId);
        fighterDetailsMap.set(fighterId, details);
        return details;
    } else {
        const details: Fighter = <Fighter>fighterDetailsMap.get(fighterId);
        return details;
    }
}

function renderSelectedFighters(selectedFighters: Fighter[]): void {
    const fightersPreview: HTMLElement = <HTMLElement>document.querySelector('.preview-container___root');
    const [playerOne, playerTwo]: Fighter[] = selectedFighters;
    const firstPreview: HTMLElement = createFighterPreview(playerOne, 'left');
    const secondPreview: HTMLElement | null = playerTwo ? createFighterPreview(playerTwo, 'right') : null;
    const versusBlock: HTMLElement = createVersusBlock(selectedFighters);

    fightersPreview.innerHTML = '';

    if (secondPreview) {
        fightersPreview.append(firstPreview, versusBlock, secondPreview);
    } else {
        fightersPreview.append(firstPreview, versusBlock);
    }
}

function createVersusBlock(selectedFighters: Fighter[]): HTMLElement {
    const canStartFight: boolean = selectedFighters.filter(Boolean).length === 2;
    const onClick: () => void = () => startFight(selectedFighters);
    const container: HTMLElement = createElement({ tagName: 'div', className: 'preview-container___versus-block' });

    const image: HTMLElement = createElement({
        tagName: 'img',
        className: 'preview-container___versus-img',
        attributes: { src: versusImg },
    });

    const disabledBtn: '' | 'disabled' = canStartFight ? '' : 'disabled';

    const fightBtn: HTMLElement = createElement({
        tagName: 'button',
        className: `preview-container___fight-btn ${disabledBtn}`,
    });

    fightBtn.addEventListener('click', onClick, false);
    fightBtn.innerText = 'Fight';
    container.append(image, fightBtn);

    return container;
}

function startFight(selectedFighters: Fighter[]): void {
    renderArena(selectedFighters);
}
