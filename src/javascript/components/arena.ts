import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { Fighter } from '../types';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';

export function renderArena(selectedFighters: Fighter[]): void {
    const root: HTMLElement = <HTMLElement>document.getElementById('root');
    const arena: HTMLElement = createArena(selectedFighters);

    root.innerHTML = '';
    root.append(arena);

    fight(
        selectedFighters[0],
        selectedFighters[1]
    )
        .then(showWinnerModal);
}

function createArena(selectedFighters: Fighter[]): HTMLElement {
    const arena: HTMLElement = createElement({ tagName: 'div', className: 'arena___root' });
    const healthIndicators: HTMLElement = createHealthIndicators(selectedFighters[0], selectedFighters[1]);
    const fighters: HTMLElement = createFighters(selectedFighters[0], selectedFighters[1]);
    
    arena.append(healthIndicators, fighters);
    return arena;
}

function createHealthIndicators(leftFighter: Fighter, rightFighter: Fighter): HTMLElement {
    const healthIndicators: HTMLElement = createElement({ tagName: 'div', className: 'arena___fight-status' });
    const versusSign: HTMLElement = createElement({ tagName: 'div', className: 'arena___versus-sign' });
    const leftFighterIndicator: HTMLElement = createHealthIndicator(leftFighter, 'left');
    const rightFighterIndicator: HTMLElement = createHealthIndicator(rightFighter, 'right');

    healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
    return healthIndicators;
}

function createHealthIndicator(fighter: Fighter, position: 'left' | 'right'): HTMLElement {
    const { name }: { name: string } = fighter;
    const container: HTMLElement = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
    const fighterName: HTMLElement = createElement({ tagName: 'span', className: 'arena___fighter-name' });
    const indicator: HTMLElement = createElement({ tagName: 'div', className: 'arena___health-indicator' });
    const bar: HTMLElement = createElement({ tagName: 'div', className: 'arena___health-bar', attributes: { id: `${position}-fighter-indicator` }});

    fighterName.innerText = name;
    indicator.append(bar);
    container.append(fighterName, indicator);

    return container;
}

function createFighters(firstFighter: Fighter, secondFighter: Fighter): HTMLElement {
    const battleField: HTMLElement = createElement({ tagName: 'div', className: `arena___battlefield` });
    const firstFighterElement: HTMLElement = createFighter(firstFighter, 'left');
    const secondFighterElement: HTMLElement = createFighter(secondFighter, 'right');

    battleField.append(firstFighterElement, secondFighterElement);
    return battleField;
}

function createFighter(fighter: Fighter, position: 'left' | 'right'): HTMLElement {
    const imgElement: HTMLElement = createFighterImage(fighter);
    const positionClassName: string = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';

    const fighterElement: HTMLElement = createElement({
        tagName: 'div',
        className: `arena___fighter ${positionClassName}`,
    });

    fighterElement.append(imgElement);
    return fighterElement;
}
