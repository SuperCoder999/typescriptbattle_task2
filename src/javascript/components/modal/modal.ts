import { createElement } from '../../helpers/domHelper';
import { Modal } from '../../types';

export function showModal(settings: Modal): void {
    const root: HTMLElement = getModalContainer();
    if (!settings.onClose) settings.onClose = () => {}; // eslint-disable-line @typescript-eslint/no-empty-function
    const modal: HTMLElement = createModal(settings); 
    
    root.append(modal);
}

function getModalContainer(): HTMLElement {
    return <HTMLElement>document.getElementById('root');
}

function createModal(settings: Modal): HTMLElement {
    const layer: HTMLElement = createElement({ tagName: 'div', className: 'modal-layer' });
    const modalContainer: HTMLElement = createElement({ tagName: 'div', className: 'modal-root' });
    const header: HTMLElement = createHeader(settings.title, <() => void>settings.onClose);

    modalContainer.append(header, settings.bodyElement);
    layer.append(modalContainer);

    return layer;
}

function createHeader(title: string, onClose: () => void): HTMLElement {
    const headerElement: HTMLElement = createElement({ tagName: 'div', className: 'modal-header' });
    const titleElement: HTMLElement = createElement({ tagName: 'span' });
    const closeButton: HTMLElement = createElement({ tagName: 'div', className: 'close-btn' });
    
    titleElement.innerText = title;
    closeButton.innerText = '×';
    
    const close: () => void = () => {
        hideModal();
        onClose();
    };

    closeButton.addEventListener('click', close);
    headerElement.append(titleElement, closeButton);
    
    return headerElement;
}

function hideModal(): void {
    const modal = document.getElementsByClassName('modal-layer')[0];
    modal?.remove();
}
