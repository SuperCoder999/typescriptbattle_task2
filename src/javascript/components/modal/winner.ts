import { Fighter } from '../../types';
import { showModal } from './modal';

export function showWinnerModal(fighter: Fighter | { name: string }): void {
    showModal({
        title: `${fighter.name} wins!`,
        bodyElement: 'Congratulations!!!',
        onClose: () => window.location.reload()
    });
}
