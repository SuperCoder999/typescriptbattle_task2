export class Controls {
    static PlayerOneAttack = 'KeyA';
    static PlayerOneBlock = 'KeyD';
    static PlayerTwoAttack = 'KeyJ';
    static PlayerTwoBlock = 'KeyL';
    static PlayerOneCriticalHitCombination: string[] = ['KeyQ', 'KeyW', 'KeyE'];
    static PlayerTwoCriticalHitCombination: string[] = ['KeyU', 'KeyI', 'KeyO'];
}
